const path = require('path');

module.exports = {
	entry: path.resolve('index'),
	output: {
		path: path.resolve('dist'),
		filename: 'index.js'
	},
	module: {
		loaders: [{
			test: new RegExp('.js$'),
			loaders: ['babel-loader'],
			exclude: new RegExp('node_modules')
		}]
	}
};
