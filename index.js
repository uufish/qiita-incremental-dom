import { observable, autorun } from 'mobx'

import {
	elementOpen, elementClose, elementVoid, text, patch
} from 'incremental-dom'

const observer = state => {
	elementOpen('span');
	text(state.count);
	elementClose('span');
	elementVoid('input', '', ['type', 'button'],
		'onclick', () => state.count -= 1,
		'value', '-1',
	);
	elementVoid('input', '', ['type', 'button'],
		'onclick', () => state.count += 1,
		'value', '+1',
	);
};

autorun(
	patch.bind(this, document.body,
		observer,
		observable({ count: 0 })
	)
);
